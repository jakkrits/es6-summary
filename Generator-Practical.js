/**
 * Created by Jakk on 9/23/2016 AD.
 */

//Create tree data structure

class Comment {
    constructor(content, children) {
        this.content = content;
        this.children = children;
    }

    *[Symbol.iterator]() {
        yield this.content;
        for(let child of this.children) {
            yield *child;
        }
    }
}

const children = [
    new Comment('good comment', []),
    new Comment('bad comment', []),
    new Comment('nevermind', [])
];

const tree = new Comment('First Topic', children);

console.log(tree);

const values = [];
for(let value of tree) {
    values.push(value);
}
console.log(values);