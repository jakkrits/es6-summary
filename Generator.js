/**
 * Created by Jakk on 9/23/2016 AD.
 */

const colors = ['red', 'green', 'blue'];

//For-Each Loop
colors.forEach(color => console.log(color));

//For-Of Loop
for (let color of colors) {
    console.log(color);
}

const numbers = [1, 2, 3, 4, 5];
let total = 0;
for (let number of numbers) {
    total += number;
}
console.log(total);

//Generator
function *generatorNumbers() {
    yield;
}
const gen = generatorNumbers();

console.log(gen.next());
console.log(gen.next());
console.log('--------------------------');

//Analogy Example
function  *shopping() {
    //STUFF ON THE SIDEWALK

    //walking down the sidewalk

    //go into the store with cash

    const stuffFromTheStore = yield 'cash';

    //then go to laundromat

    const cleanClothes = yield 'laundry';

    //walking back home
    //return stuffFromTheStore;
    return [stuffFromTheStore, cleanClothes];
}

//STUFF IN THE STORE
const genShop = shopping(); //note this don't invoke a function
console.log(genShop.next());//this actually first run the function -> now you go into the store with cash
//returns { value: 'cash', done: false }
console.log(genShop.next('groceries'));
// this continue the function again -> now you go get laundry
//returns { value: 'laundry', done: false }
console.log(genShop.next('laundry'));
//{ value: [ 'groceries', 'laundry' ], done: true }


//GENERATOR - Real world example
console.log('--------------------');
function *myColors() {
    yield 'red';
    yield 'green';
    yield 'blue';
    yield 'black';
}

const colorArray = [];
for (let color of myColors()) {
    console.log(color);
    colorArray.push(color);
}
console.log(colorArray);
console.log('.....................');

//ITERATES THROUGH ONLY SOME INTERESTED PROPERTIES
/*
const testingTeam = {
    lead: 'Jane',
    tester: 'Bill'
};

const engineeringTeam = {
    testingTeam,
    size: 3,
    department: 'Engineering',
    lead: 'Jill',
    manager: 'Alice',
    engineer: 'Dave'
};

function *TeamIterator(team) {
    yield team.lead;
    yield team.manager;
    yield team.engineer;
    //another approach if not use another generator
    // yield team.testingTeam.lead;
    //yield team.testingTeam.tester;

    const teamGen = TestingIterator(team.testingTeam);
    yield *teamGen;
}

function *TestingIterator(team) {
    yield team.lead;
    yield team.tester;
}

const names = [];
for (let name of TeamIterator(engineeringTeam)) {
    names.push(name);
}
console.log(names);
*/

//USE OF SYMBOL.ITERATOR
/*
const testingTeam = {
    lead: 'Jane',
    tester: 'Bill',
    [Symbol.iterator]: function *() {
        yield this.lead;
        yield this.tester;
    }
};

const engineeringTeam = {
    testingTeam,
    size: 3,
    department: 'Engineering',
    lead: 'Jill',
    manager: 'Alice',
    engineer: 'Dave'
};

function *TeamIterator(team) {
    yield team.lead;
    yield team.manager;
    yield team.engineer;
    yield *team.testingTeam;
}

const names = [];
for (let name of TeamIterator(engineeringTeam)) {
    names.push(name);
}
console.log(names);
*/

//ITERATE DIRECTLY
const testingTeam = {
    lead: 'Jane',
    tester: 'Bill',
    [Symbol.iterator]: function *() {
        yield this.lead;
        yield this.tester;
    }
};

const engineeringTeam = {
    testingTeam,
    size: 3,
    department: 'Engineering',
    lead: 'Jill',
    manager: 'Alice',
    engineer: 'Dave',
    [Symbol.iterator]: function *() {
        yield this.lead;
        yield this.manager;
        yield this.engineer;
        yield * this.testingTeam;
    }
};

const names = [];
for (let name of engineeringTeam) {
    names.push(name);
}
console.log(names);