/**
 * Created by Jakk on 9/23/2016 AD.
 */
let promise = new Promise((resolve, reject) => {
    //resolve();
    //reject();

    setTimeout(() => {
        reject();
    }, 3000);
});

promise
    .then(() => console.log('finally finished!!!!'))
    .then(() => console.log('then i am run'))
    .catch(() => console.log('something is wrong'));

//FETCH -> Promise Native Browser Thing
const url = 'https://jsonplaceholder.typicode.com/posts/';
//1. call response with json() method
//2. get data from that return json
fetch(url)
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.log('BAD FETCH', error));

//should use AXIOS instead